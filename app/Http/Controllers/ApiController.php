<?php

namespace App\Http\Controllers;

use App\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    /**
     * GRABBER TWEET DANA INDONESIA
     * start function grabber tweet dana
    */
    public function index() {
        $data = array();
        $lastID = '';
        $query = $this->getSearch();
        foreach($query->statuses as $query) {
            $data[] = array(
                'created_at' => $query->created_at,
                'id' => $query->id,
                'text' => $query->text,
                'user' => $query->user->screen_name,
            );
            $lastID = $query->id;
        }

        $queryBy = $this->getSearchBy($lastID);
        foreach($queryBy->statuses as $queryBy) {
            $data[] = array(
                'created_at' => $queryBy->created_at,
                'id' => $queryBy->id,
                'text' => $queryBy->text,
                'user' => $queryBy->user->screen_name,
            );
            $lastID = $queryBy->id;
        }

        $filename = 'twitter_json_' . date('Y_m_d_H_i_s') . ('.json');
        Storage::disk('public')->put($filename, json_encode($data));
    }

    // TEST DATA FROM FILE
    public function test() {
        $rooms = json_decode(Storage::disk('public')->get('twitter_json.json'));
        $data = '';
        $no = 1;
        foreach((array)$rooms as $fm) {
            $data .= $no++."-".$fm->created_at."<br>";
        }

        echo $data;
    }
    // END TEST DATA FROM FILE

    // DANA INDONESIA
    /**
    * grabber tweets
    */
    public function getSearch() {
        header('Content-Type: application/json');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q=%40danawallet&count=100",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAAs5%2FAAAAAAA%2BFhxtLDRr2AuKh5zdIHTczhg0Jg%3DltF0dqGzLFlmXH9wjI8HkO1gEzGlnCYUegwIOVVu1Umn8Yi1sX",
                "Cookie: personalization_id=\"v1_zm5erBgqFPAd3J9x7Tvfhw==\"; guest_id=v1%3A159633781952627458; lang=en"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode(str_replace('\n', ' ', $response));
    }

    /**
    * last id from search method
    * @param int $id
    */
    public function getSearchBy($id) {
        header('Content-Type: application/json');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q=%40danawallet&max_id=$id&count=100",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAAs5%2FAAAAAAA%2BFhxtLDRr2AuKh5zdIHTczhg0Jg%3DltF0dqGzLFlmXH9wjI8HkO1gEzGlnCYUegwIOVVu1Umn8Yi1sX",
                "Cookie: personalization_id=\"v1_zm5erBgqFPAd3J9x7Tvfhw==\"; guest_id=v1%3A159633781952627458; lang=en"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode(str_replace('\n', ' ', $response));
    }
    // DANA INDONESIA
    /**
     * GRABBER TWEET DANA INDONESIA
     * END function grabber tweet dana
    */

    /**
    * keyword search tweets
    * @param string $keywords
    */
    // CUSTOM SEARCH
    public function getTweet($keywords) {
        if (strpos($keywords, '@') !== false) {
            $keywords = str_replace('@', '%40', $keywords);
        } else if (strpos($keywords, '#') !== false){
            $keywords = str_replace('#', '%23', $keywords);
        } else {
            $keywords = $keywords;
        }
        echo $keywords;
        $data = array();
        $lastID = '';
        $query = $this->getCustomSearch($keywords);
        foreach($query->statuses as $query) {
            $data[] = array(
                'created_at' => $query->created_at,
                'id' => $query->id,
                'text' => $query->text,
                'user' => $query->user->screen_name,
            );
            $lastID = $query->id;
        }

        $queryBy = $this->getCustomSearchBy($lastID, $keywords);
        foreach($queryBy->statuses as $queryBy) {
            $data[] = array(
                'created_at' => $queryBy->created_at,
                'id' => $queryBy->id,
                'text' => $queryBy->text,
                'user' => $queryBy->user->screen_name,
            );
            $lastID = $queryBy->id;
            $lastTime = $queryBy->created_at;
        }

        $filename = 'twitter_json_' . date('Y_m_d_H_i_s') . ('.json');
        Storage::disk('grabber')->put($filename, json_encode($data));
    }

    /**
    * grabbber tweets
    * @param string $keywords
    */
    public function getCustomSearch($keywords) {
        header('Content-Type: application/json');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q=$keywords&lang=id&count=100",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAAs5%2FAAAAAAA%2BFhxtLDRr2AuKh5zdIHTczhg0Jg%3DltF0dqGzLFlmXH9wjI8HkO1gEzGlnCYUegwIOVVu1Umn8Yi1sX",
                "Cookie: personalization_id=\"v1_zm5erBgqFPAd3J9x7Tvfhw==\"; guest_id=v1%3A159633781952627458; lang=en"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode(str_replace('\n', ' ', $response));
    }

    /**
    * grabbber tweets + last id from the last id
    * @param string $keywords
    * @param int $id - last id from the last method
    */
    public function getCustomSearchBy($id, $keywords) {
        header('Content-Type: application/json');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q=$keywords&max_id=$id&lang=id&count=100",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAAs5%2FAAAAAAA%2BFhxtLDRr2AuKh5zdIHTczhg0Jg%3DltF0dqGzLFlmXH9wjI8HkO1gEzGlnCYUegwIOVVu1Umn8Yi1sX",
                "Cookie: personalization_id=\"v1_zm5erBgqFPAd3J9x7Tvfhw==\"; guest_id=v1%3A159633781952627458; lang=en"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode(str_replace('\n', ' ', $response));
    }
    // CUSTOM SEARCH
}
